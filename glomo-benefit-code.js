Polymer({

  is: 'glomo-benefit-code',

  behaviors: [
    CellsBehaviors.i18nBehavior
  ],

  properties: {
    /**
    * @desc modal full screen
    * @type {Boolean}
    */
    fullHeight: Boolean,

    /**
    * @desc title modal
    * @type {String}
    */
    title: String,
    /**
    * @desc subtitle modal
    * @type {String}
    */
    subtitle: String,
    /**
    * @desc show input
    * @type {Boolean}
    */
    enableInput: Boolean,
    /**
    * @desc show text inside the input
    * @type {String}
    */
    labelInput: String,
    /**
    * @desc icon inside input
    * @type {String}
    */
    iconInput: {
      type: String,
      value: 'coronita:close'
    },
    /**
    * @desc length the inpunt
    * @type {Number}
    */
    inputLength: Number,
    inputValue: {
      type: String,
      observer: '_getInputValue'
    },
    allowedChars: {
      type: String,
      value: '[0-9a-zA-Z]'
    },
    message: String,
    primaryButton: {
      type: Boolean,
      value: false
    },
    primaryButtonLabel: {
      type: String,
      value: 'Aceptar'
    },
    buttonDisable: {
      type: Boolean,
      value: false
    },
    buttonDisableLabel: String,
    cancelButton: Boolean,
    cancelButtonLabel: String,
      
    classCancelButton: String,
    opened: {
      type: Boolean,
      notify: true,
      value: false,
      reflectToAttribute: true,
      observer: '_checkOpen'
    },
    inputMessage: String,
    infoMessage: String,
    inputErrorMessage: String,
    classError: String,
  },
  success(description) {
    console.log(description);
    let content = this.inputValue;
    this.dispatchEvent(new CustomEvent('benefit-success',{
      detail: {
        benefitCoupon: false,
        codeTitle: content,
        codeDescription: description
      }
    }));
    this.close();
  },
  warning(error) {
    this.infoMessage = this.inputMessage;
    this.inputMessage = '';
    this.inputErrorMessage = error;
    this.classError = 'warning';
    this.buttonDisable= false;
  },
  close() {
    this.opened= false;
    this._reset();
  },
  open() {
    this.opened =true;
  },
  _reset() {
    this.inputValue = '';
    this.infoMessage = '';
    this.inputMessage = '';
  },
  _getInputValue(e) {
    if(e.length < this.inputLength && this.classError) {
      this.classError = '';
      this.inputErrorMessage='';
      this.inputMessage = this.infoMessage;
      // this.buttonDisable = true;
    } else 
    if (e.length === this.inputLength) {
      this._action(true);
    } else {
      this._action(false);
    }
  },
  _checkOpen() {
    this.$.modal[this.opened ? 'open' : 'close']();
  },
  _action(buttonDisable) {
    [ buttonDisable ? this.buttonDisable = true : this.buttonDisable = false ]
  },
  _setValue() {
    console.log('si se activa');
    let content = this.inputValue;
    this.dispatchEvent(new CustomEvent('code-validate', {
      detail: {
        content
      }
    }));
  }
});